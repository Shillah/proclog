(ns proclog.core-test
  (:require [clojure.test :refer :all]
            [proclog.core :refer :all]))

(deftest a-test
  (testing "Some things"
    (<- p [x] (= x 3))
    (assert (= (proof (p :x) (p :y)) {:x 3, :y 3}))))
