(ns proclog.core
  (:require [proclog.util :as util]
            [proclog.unify :as unify]
            [clojure.string :as str]))

;;; 

(def ^:dynamic *special-functions*
  #{'is 'or})

(def ^:dynamic *replaced-functions*
  "A map consiting of names of functions to replace and their replacement."
  {'= `unify, 'list `p__internal_list})

(defn undefined-variables [params body]
  "Given a list of defined variables `params` and a function body `body` this function returns a set of used but undefined variable names."
  (into #{} (mapcat (fn [x]
                      (cond
                        (seq? x) (undefined-variables params (rest x))
                        :else (if (and (not (util/member? x params)) (symbol? x))
                                (list x) nil))) body)))

(defn undefined-variable-definitions
  "Automatically creates variable definitions for the found undefinied variables."
  [vars-to-define]
  (into [] (mapcat (fn [to-def] (list to-def `(util/get-unused-keyword))) vars-to-define)))

(declare transform-body)

(defn choice [bodies]
  (if bodies
    (let [can-choice (first bodies)     ; canonical choice
          bt-points (rest bodies)]
      `(~@(map (fn [bt-point]
                 (let [state (gensym)]
                   `(backtrack-point (fn [~state] (bt-do ~state ~@(transform-body bt-point)))))) bt-points)
        ~@(transform-body can-choice)))
    `(identity)))

;;; This does not work since it uses the old mgu value.
;;; This needs to be integrated into bt->
(defn add-lookups [mgu form]
  (cond
    (symbol? form) `(unify/mgu-lookup ~mgu ~form)
    (seq? form) (cons (first form) (map #(add-lookups mgu %) (rest form)))
    true form))

(defn add-unquoting [form]
  (cond
    (symbol? form) `(unquote ~form)
    (seq? form) (cons (first form) (map add-unquoting (rest form)))
    true form))

(defn transform-args [args]
  (cond
    (symbol? args) args
    (list? args) `(list ~@(map transform-args args))
    true args))

(defn transform-form [form]
  (let [fun (first form)
        args (rest form)]
    (cond
      ;; special forms
      (= fun 'or) (choice args)
      (= fun 'is) (do
                    (assert (= (count args) 2))
                    (let [vals (gensym)]
                      `(((fn [~vals]
                           (unify ~vals
                                  ~(first args)
                                  ~(add-lookups `(:mgu ~vals) (second args))))))))
      ;; normal form
      true (list (cons (get *replaced-functions* fun fun) (map transform-args args))))))

(defn transform-body [body]
  (mapcat transform-form body))

(defn undefined-functions
  "Searches through body and returns a list of used but undefined functions."
  [body]
  (for [form body
        :let [fun-name (first form)
              path (resolve fun-name)]
        :when (not (or (contains? *special-functions* fun-name)
                       (contains? *replaced-functions* fun-name)
                       (and path (bound? path))))]
    fun-name))

(defn body-to-functions [body]
  (cons 'list (map (fn [form]
                     (let [state (gensym)]
                       `(fn [~state] ~(if (seq? form)
                                       `(~(first form) ~state ~@(rest form))
                                       (list form state)))))
                   body)))

(defn pop-one [forms]
  (if (empty? (first forms))
    forms
    (conj (rest forms) (rest (first forms)))))

(defn backtrack [state]
  (let [bt-info (gensym)]
    `(if (empty? (:bt-stack ~state))
       ~state
       (let [~bt-info (first (:bt-stack ~state))]
         (recur {:mgu (:mgu ~bt-info)
                 :forms (:forms ~bt-info)
                 :bt-stack (rest (:bt-stack ~state))})))))

(defn rem-context [state]
  (update state :forms rest))

(defn backtrack-concat [n forms]
  (assert (and (integer? n) (<= 0 n)))
  (conj (drop (inc n) forms) (reduce concat (take (inc n) forms))))

(defmacro bt-do [state & body]
  (let [st (gensym)
        forms (gensym)
        bt-point (gensym)]
    `(loop [~st (update ~state :forms conj ~(body-to-functions body))]
;       (clojure.pprint/pprint ~st)
       (let [~forms (first (:forms ~st))]
         (cond
           (not (:mgu ~st)) (if (empty? (:bt-stack ~st))
                              ~st
                              (let [~bt-point (first (:bt-stack ~st))]
                                (recur {:mgu (:mgu ~bt-point),
                                        :forms (backtrack-concat (- (:depth ~bt-point) (:depth ~st))
                                                                 (:forms ~bt-point)),
                                        :bt-stack (rest (:bt-stack ~st)),
                                        :depth (:depth ~st)})))
           (empty? ~forms) (update ~st :forms rest)
           true (recur (update ((first ~forms) (-> ~st
                                                   (update :depth inc)
                                                   (update :forms pop-one))) :depth dec)))))))


(defn unify [state x y]
  (update state :mgu proclog.unify/unify x y))

(defn backtrack-point [state bt]
  (let [forms (:forms state)]
    ;; we do (rest (first forms)) instead of (first forms) since the first element of (first forms)
    ;; will be the default case in `or`
    (update state :bt-stack conj {:mgu (:mgu state),
                                  :forms (conj (rest forms) (conj (rest (first forms)) bt)),
                                  :depth (dec (:depth state))})))
