(ns proclog.core2
  (:require [proclog.util :as util]
            [proclog.unify :as unify]))

(declare print-mgu yes-or-no)

(def ^:dynamic *special-functions*
  #{'is 'or})

(def ^:dynamic *replaced-functions*
  "A map consiting of names of functions to replace and their replacement."
  {'= 'proclog.core2/unify, 'list 'proclog.core2/p__internal_list})

(defn undefined-variables [params body]
  "Given a list of defined variables `params` and a function body `body` this function returns a set of used but undefined variable names."
  (into #{} (mapcat (fn [x]
                      (cond
                        (seq? x) (undefined-variables params (rest x))
                        :else (if (and (not (util/member? x params)) (symbol? x))
                                (list x) nil))) body)))

(defn undefined-variable-definitions
  "Automatically creates variable definitions for the found undefinied variables."
  [vars-to-define]
  (into [] (mapcat (fn [to-def] (list to-def `(util/get-unused-keyword))) vars-to-define)))

(declare transform-body)

(defn choice [bodies]
  (if bodies
    (let [can-choice (first bodies)     ; canonical choice
          bt-points (rest bodies)]
      `(~@(map (fn [bt-point]
                 (let [m-and-bt (gensym)]
                   `(backtrack-point (fn [~m-and-bt] (bt-> ~m-and-bt ~@(transform-body bt-point)))))) bt-points)
        ~@(transform-body can-choice)))
    `(identity)))

;;; This does not work since it uses the old mgu value.
;;; This needs to be integrated into bt->
(defn add-lookups [mgu form]
  (cond
    (symbol? form) `(unify/mgu-lookup ~mgu ~form)
    (seq? form) (cons (first form) (map #(add-lookups mgu %) (rest form)))
    true form))

(defn add-unquoting [form]
  (cond
    (symbol? form) `(unquote ~form)
    (seq? form) (cons (first form) (map add-unquoting (rest form)))
    true form))

(defn transform-args [args]
  (cond
    (symbol? args) args
    (list? args) `(list ~@(map transform-args args))
    true args))

(defn transform-form [form]
  (let [fun (first form)
        args (rest form)]
    (cond
      ;; special forms
      (= fun 'or) (choice args)
      (= fun 'is) (do (assert (= (count args) 2)) (let [vals (gensym)]
                                                    `(((fn [~vals]
                                                         (unify ~vals
                                                                ~(first args)
                                                                ~(add-lookups `(first ~vals) (second args))))))))
      ;; normal form
      true (list (cons (get *replaced-functions* fun fun) (map transform-args args))))))

(defn transform-body [body]
  (mapcat transform-form body))

(defn undefined-functions
  "Searches through body and returns a list of used but undefined functions."
  [body]
  (for [form body
        :let [fun-name (first form)
              path (resolve fun-name)]
        :when (not (or (contains? *special-functions* fun-name)
                       (contains? *replaced-functions* fun-name)
                       (and path (bound? path))))]
    fun-name))

(defn do-proof [mgu forms bt-stack]
  (cond
    (= mgu false) (if (empty? bt-stack)
                    false
                    (let [[[mgu forms] & rest] bt-stack]
                      (recur mgu forms rest)))
    forms (let [[f & fs] forms
                [mgu forms bt-stack] (f [mgu fs bt-stack])]
            (recur mgu forms bt-stack))
    true (do
           (print-mgu mgu)
           (if (or (empty? bt-stack) (yes-or-no))
             true
             (let [[[mgu forms] & rest] bt-stack]
               (recur mgu forms rest))))))

(defmacro bt-> [vals & body]
  (loop [vals vals, forms body]
    (if (empty? forms)
      vals
      (let [form (first forms)
            threaded (let [v (gensym)]
                       `(let [~v ~vals]
                          (if (first ~v)
                            ~(if (seq? form)
                               (with-meta `(~(first form) ~v ~@(rest form)) (meta form))
                               (list form v))
                            ~v)))]
        (recur threaded (next forms))))))

(defn conj* [coll x]
  (if (empty? coll)
    x
    (concat coll (list x))))

(defmacro <- [name [& params] & body]
  (let [vars (undefined-variables params body)
        funs (undefined-functions body)
        mgu-and-bt-stack (gensym)
        declaration `(defn ~name  ~(into [mgu-and-bt-stack] params))
        body `(bt-> ~mgu-and-bt-stack ~@(transform-body body))
        declare-funs (if (empty? funs) nil `(do (declare ~@funs)))
        declare-vars (if (empty? vars) nil `(let ~(undefined-variable-definitions vars)))]
    ;; This may seem weird but it is done to make
    ;; the outputted code much clearer to read
    ;; since (let [] X) and (do Y)
    ;; are reduced to just X and Y.
    (->> body
         (conj* declare-vars)
         (conj* declaration)
         (conj* declare-funs))))


(defn unify [[mgu forms bt-stack] x y]
  [(unify/unify mgu x y) forms bt-stack])

(defn backtrack-point [[mgu forms bt-stack] bt]
  [mgu forms (conj bt-stack [mgu (conj forms bt)])])















(defn print-mgu [mgu]
  (if mgu
      (doall (map (fn [[var value]] (println (name var) " = " value)) mgu)))
  (boolean mgu))

(defn yes-or-no []
  (println "Yes / No:")
  (loop [answer (clojure.string/lower-case (read-line))]
    (cond
      (= answer "yes") true
      (= answer "no") false
      true (do (println "Enter \"Yes\" or \"No\":")
               (recur (clojure.string/lower-case (read-line)))))))

(defn do-proof2 [form]
  (loop [mgu false bt-stack (list [form {}])]
    (if mgu
      (do (print-mgu mgu)
          (if (and (seq bt-stack)
                   (not (yes-or-no)))
            (let [form (first (first bt-stack))
                  mgu (second (first bt-stack))
                  bt-stack (rest bt-stack)
                  res (form [mgu bt-stack])]
              (recur (first res) (second res)))
            true))
      
      (if (empty? bt-stack)
        ;; if we can not backtrack anymore, then we can
        ;; not proof the query
        false
        ;; if the backtrack stack is not empty
        ;; we just backtrack to the last
        ;; backtracking point
        (let [form (first (first bt-stack))
              mgu (second (first bt-stack))
              bt-stack (rest bt-stack)
              res (form [mgu bt-stack])]
          (recur (first res) (second res)))))))

(defmacro proof [& tests]
  (let [mgu-and-bt-stack (gensym)]
    `(do-proof {} (list (fn [~mgu-and-bt-stack] (bt-> ~mgu-and-bt-stack ~@tests))) '())))
