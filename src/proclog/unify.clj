(ns proclog.unify
  (:use proclog.util))

(declare unify)

(defn- compatible? [x y]
  (and (list? x) (list? y) (= (count x) (count y))))

(defn replace-var
  "Replaces all occurences of the keyword `x` in `form` with `y`"
  [form x y]
  (assert (keyword? x))
  (cond
    (keyword? form) (if (= form x) y form)
    (seq? form) (map #(replace-var % x y) form)
    true form))

(defn mgu-apply [mgu form]
  (reduce (fn [f [var val]] (replace-var f var val)) form mgu))

(defn mgu-assoc [mgu x y]
  (if (contains? mgu x)
    (unify mgu (mgu x) y)
    (let [new-mgu (into {} (for [[k v] mgu] [k (replace-var v x y)]))]
      (assoc new-mgu x (mgu-apply new-mgu y)))))

(defn mgu-lookup [mgu var]
;  (println "Find " var " in " mgu " and return " (get mgu var var))
  (get mgu var var))

(defn unify
  ([mgu x y]
   (cond
     (keyword? x) (mgu-assoc mgu x y)
     (keyword? y) (mgu-assoc mgu y x)
     (compatible? x y) (reduce #(unify %1 %2) mgu (map vector x y))
     (= x y) mgu
     true false))
  ([mgu [x y]]
   (unify mgu x y)))
